/**
 * 1.1 tic toc
 */
const ticToc = function ticToc() {
  const letters = ['i', 'o'];

  console.log(`t${letters[this.letter]}c`, this.letter);

  this.letter ^= 1;
}.bind({ letter: 0 });


// 1.2
// 1.3
// 1.4
// 1.5


/**
 * 2.1 map implementation with reduce
 *
 * @param  {array}    arr  The collection
 * @param  {function} cb   The callback
 * @return {array}    The result
 */
function map(arr, cb) {
  return arr.reduce(function (accum, value) {
    return accum.concat(cb(value));
  }, []);
};


/**
 * 2.2 remove duplicates
 *
 * @param  {array}  arr   The collection
 * @param  {*}      prop  The property
 * @return {array}  The result
 */
function removeDuplicates(arr, prop) {
  return arr.filter((obj, pos, arr) => {
    return arr.map(item => item[prop]).indexOf(obj[prop]) === pos;
  });
}


/**
 * 2.3 sum filter values
 *
 * @param  {array}  arr  The collection
 * @return {array}  The result
 */
function sum(arr) {
  return arr.filter(function (value) {
    return typeof value === 'number';
  })
  .reduce(function (accum, value) {
    return accum + value;
  });
}


/**
 * 2.4 filter implementation with reduce
 *
 * @param  {array}    arr  The collection
 * @param  {function} cb   The callback
 * @return {array}    The result
 */
function filter(arr, cb) {
  return arr.reduce((accum, value) => {
    if (fn(value)) {
      return true;
    }

    return accum;
  }, false);
}


/**
 * 2.5 sum flatten map
 *
 * @param  {array}  arr  The collection
 * @return {array}  The result
 */
function flatten(arr) {
  return arr.reduce(function (accum, value) {
    return accum.concat(Array.isArray(value) ? flatten(value) : value);
  }, []);
};

function run(arr) {
  return flatten(arr).reduce(function (accum, value) {
    return accum + value;
  }, 0);
}


/**
 * 3.1 once
 *
 * @param  {function} cb  The callback
 * @return {function} The closure
 */
function once(cb) {
  let response = null;

  return function (...args) {
    if (cb) {
      response = cb.apply(this, args);
      cb = null;
    }

    return response;
  };
}


/**
 * 3.2 compose
 *
 * @param  {function}  cb1  The callback 1
 * @param  {function}  cb2  The callback 2
 * @return {function}  The closure
 */
function compose(cb1, cb2) {
  return function (arr, _cb1, _cb2) {
    return cb1(_cb1, cb2(_cb2, arr));
  };
}

function fn1(item) {
  return item.name;
}

function fn2(item) {
  return item.id % 2 === 0;
}

function filter(cb, arr) {
  return arr.filter(cb);
}

function map(cb, arr) {
  return arr.map(cb);
}


/**
 * 3.3 debounce
 *
 * @param  {function} cb    The callback
 * @param  {number}   delay The delay time
 * @return {function} The closure
 */
function debounce(cb, delay) {
  let timeout = null;

  return function (...args) {
    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(function() {
      cb.apply(this, args);

      timeout = null;
    }, delay);
  }
}


/**
 * 3.4 throttle
 *
 * @param  {function} cb      The callback
 * @param  {number}   period  The perid time
 * @return {function} The closure
 */
function throttle(cb, period) {
  let lastCall = 0;

  return function (...args) {
    const now = new Date().getTime();

    if ((now - lastCall) < period) {
      return;
    }

    lastCall = now;

    return cb.apply(this, args);
  }
}


/**
 * 3.6 repeat
 *
 * @param  {function} cb  The callback
 * @param  {number}   n   The number of times to repeat
 * @return {function} The closure
 */
function repeat(cb, n) {
  let accum = 0;

  return function iterate(...args) {
    if (n <= 0) {
      return accum;
    }

    n = n - 1;

    accum = accum + cb(...args);

    return iterate(...args);
  };
}


/**
 * 3.7 currying
 *
 * @param  {function} cb  The callback
 * @return {function} The closure
 */
function currying(cb) {
  const n = cb.length;

  return function (...args) {
    if (n <= 1) {
      return cb.apply(this, args);
    }

    return currying(cb.bind(this, ...args));
  };
}


/**
 * 3.8 memoize
 *
 * @param  {function} cb  The callback
 * @return {function} The closure
 */
function memoize(cb) {
  const storage = {};

  return function (...args) {
    if (storage[args] === undefined) {
      storage[args] = cb.apply(this, args);
    }

    return storage[args];
  }
}
