/**
 * 1. Completar la siguiente función para
 *    que devuelva alternadamente tic y toc
 *
 *    Ej:
 *      ticToc() // tic
 *      ticToc() // toc
 *      ticToc() // tic
 *      ticToc() // toc
 */
function ticToc() {}


/**
 * 2. Crear objeto con la siguiente función
 *    constructora y añadir al contexto los
 *    parámetros recibidos
 *
 * @class Media (name)
 * @param {string}  title     The title
 * @param {string}  duration  The duration
 */
function Media(title, duration) {
  this.isPlaying = false;
}


/**
 * 3. Agregar los siguientes métodos al
 *    objeto anterior y que puedan ser
 *    accedidos por todas las instancias
 */
function start() {
  this.isPlaying = true;
}

function stop() {
  this.isPlaying = false;
}


/**
 * 4. Crear dos nuevos objetos que hereden
 *    del objeto Media
 */
function Song(title, artist, duration) {}

function Movie(title, year, duration) {}


/**
 * 5. Transformar las soluciones anteriores
 *    a clases
 */
class Media {}
class Song {}
class Movie {}
