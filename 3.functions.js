/**
 * 1. Escribir una función que reciba
 *    otra función como primer argumento y
 *    permita ejecutarla tan solo una vez
 *
 *    Ej:
 *      const iden = (num) => num;
 *      const justOnce = once(iden);
 *
 *      justOnce(1); // 1
 *      justOnce(2); // undefined
 *      justOnce(3); // undefined
 *      justOnce(4); // undefined
 *      ...
 *
 * @param {function}  cb  The callback
 */
function once(cb) {}

/**
 * 2. Escribir una función que componga
 *    los métodos map y filter para extraer
 *    los ids impares de una colección
 *
 *    Ej:
 *      const collection = [
 *        { id: 1, name: 'juan' },
 *        { id: 2, name: 'pedro' },
 *        { id: 3, name: 'hugo' },
 *        { id: 4, name: 'paco' },
 *        { id: 5, name: 'luis' },
 *      ];
 *
 * @param {function}  map     The map callback
 * @param {function}  filter  The filter callback
 */
function compose(map, filter) {}


/**
 * 3. Escribir una función que reciba
 *    otra función como primer argumento y
 *    permita ejecutarla con un tiempo de
 *    retraso (ej: 100 ms)
 *
 * @param {function}  cb     The callback
 * @param {integer}   delay  The time to delay
 */
function debounce(cb, delay) {}


/**
 * 4. Escribir una función que reciba
 *    otra función como primer argumento y
 *    permita ejecutarla limitadas veces
 *    dentro de un periodo de tiempo
 *    (ej: 100 ms)
 *
 * @param {function}  cb      The callback
 * @param {integer}   period  The period
 */
function throttle(cb, period) {}


/**
 * 5. Buscar e implementar casos en donde
 *    se puedan utilizar las funciones
 *    debounce y throttle
 */


/**
 * 6. Escribir una función que reciba
 *    otra función como primer argumento
 *    y permita ejecutarla n veces.
 *
 * @param {function}  cb  The callback
 * @param {integer}   n   The number of times
 */
function repeat(cb, n) {}


/**
 * 7. Escribir una función que reciba
 *    otra función, con argumentos indefinidos,
 *    como primer argumento y la transforme
 *    a currying.
 *
 *    Ej:
 *      function add(a, b, c, ...) {
 *        return a + b + c + ...;
 *      }
 *
 *      const curr = currying(add);
 *
 *      curr(1)(2)(3)(.)(.)...;
 *
 * @param {function}  cb  The callback
 */
function currying(cb) {}


/**
 * 8. Escribir una función que reciba
 *    otra función como primer argumento y
 *    permita guardar en caché los valores
 *    que retorna la función proporcionada
 *
 * @param {function}  cb  The callback
 */
function memoize(cb) {}
