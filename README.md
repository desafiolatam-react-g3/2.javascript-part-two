INSTRUCCIONES
---

### Instalar dependencias
```
npm install
```

### Correr scripts con babel
```
npm run babel 1.objects.js

npm run babel 2.methods.js

npm run babel 3.functions.js
```

### Construir scripts con babel
```
npm run build
```

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
