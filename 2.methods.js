//  Solo se pueden utilizar: map, filter,
//  reduce y concat

/**
 * 1. Implementar map utilizando reduce
 *
 *    Ej:
 *      const nums = [1, 2, 3, 4, 5];
 *      const output = map(nums, function (item) {
 *        return item * 2;
 *      });
 */


/**
 * 2. Eliminar todos los valores duplicados
 *    de una colección
 *
 *    Ej:
 *      const collection = [
 *        { id: 0 },
 *        { id: 2 },
 *        { id: 6 },
 *        { id: 2 },
 *        { id: 3 },
 *        { id: 0 },
 *      ];
 */


/**
 * 3. Sumar los valores númericos del
 *    siguiente arreglo
 */
const values = [0, 1, undefined, -10, '0', 42];


/**
 * 4. Verificar si el siguiente arreglo
 *    contiene el valor 42
 */
const verify = [101, 67, 213, 94, 59, 42, 62, 40];


/**
 * 5. Sumar todos valores de la siguiente
 *    estructura
 */
const nested = [1, 2, [3, 4], [[5], [6, 7]], 8];
